# BE ASI some help 

## 1. Link to gitlab 

[Click here](https://gricad-gitlab.univ-grenoble-alpes.fr/dallamum/image-processing-course-asi-2a)

## 2. How to launch jupyter or jupyter-lab with anaconda powershell prompt

- a. In windows search bar, look for "anaconda powershell prompt", excecute it
- b. Go to the location in windows where you've extracted the BE with the command **cd your_path**
- c. Start jupyter notebook by excecuting the command **jupyter notebook** or jupyter-lab (better) with the command **jupyter-lab**

## 3. How to increase cell in jupyter notebook (No use if your are using jupyter lab)

In a cell of your notebook copy/paste these two lines:

from IPython.display import display, HTML<br />
display(HTML("<style>.container { width:90% !important; }</style>"))